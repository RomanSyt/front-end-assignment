import { useState, useEffect, useCallback } from 'react';

export const useFetch = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<null | string>(null);

  const onRequest = useCallback(async (url: string) => {
    setIsLoading(true);
    try {
      const response = await fetch(url);
      const data = await response.json();

      if (!response.ok) {
        throw Error('Something went wrong!');
      }

      return data;
    } catch (e: unknown) {
      if (typeof e === 'string') {
        setError(e.toUpperCase());
      } else if (e instanceof Error) {
        setError(e.message);
      }
    }
    setIsLoading(false);
  }, []);

  const clearError = useCallback(() => setError(null), []);

  return { onRequest, isLoading, error, clearError };
};
