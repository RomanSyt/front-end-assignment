export enum RecipesEndpoints {
  getAllMealCategories = 'https://www.themealdb.com/api/json/v1/1/categories.php',
  getMealsByCategories = 'https://www.themealdb.com/api/json/v1/1/filter.php?c=',
  getMealById = 'https://www.themealdb.com/api/json/v1/1/lookup.php?i=',
}
