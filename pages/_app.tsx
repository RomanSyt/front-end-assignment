import type { AppProps } from 'next/app';
import Header from '../components/Header';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import { LOGO_WIDTH } from '../components/Header/constants';

declare module '@mui/material/styles' {
  interface BreakpointOverrides {
    xs: false;
    sm: false;
    md: false;
    lg: false;
    xl: false;
    mobile: true;
    tablet: true;
    ltablet: true;
    laptop: true;
    desktop: true;
    mdesktop: true;
    ldesktop: true;
    xldesktop: true;
  }
}

const theme = createTheme({
  breakpoints: {
    values: {
      mobile: 0,
      tablet: 640,
      ltablet: 900,
      laptop: 1024,
      desktop: 1200,
      mdesktop: 1300,
      ldesktop: 1400,
      xldesktop: 1536,
    },
  },
  components: {
    MuiToolbar: {
      styleOverrides: {
        root: {
          paddingLeft: LOGO_WIDTH + 16,
        },
      },
    },
    MuiBreadcrumbs: {
      styleOverrides: {
        root: {
          '.MuiBreadcrumbs-li p, .MuiBreadcrumbs-li a': {
            textTransform: 'uppercase',
          },
        },
      },
    },
  },
});

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header />
      <Component {...pageProps} />
    </ThemeProvider>
  );
}
