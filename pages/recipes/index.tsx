import { GetStaticProps, NextPage } from 'next';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Breadcrumbs from '../../components/Breadcrumbs';
import { RecipesEndpoints } from '../../hooks/useFetch/enumerators';
import { Categories, Category } from '../../components/Cards/interface';
import Cards from '../../components/Cards';
import Card from '../../components/Card';

export const getStaticProps: GetStaticProps = async () => {
  const response = await fetch(RecipesEndpoints.getAllMealCategories);
  const categories = await response.json();

  return {
    props: {
      categories,
    },
  };
};

interface RecipesProps {
  categories: Categories;
}

const Recipes: NextPage<RecipesProps> = ({ categories }) => {
  const breadcrumbs = [
    <Typography key="1" color="text.primary">
      Recipes
    </Typography>,
  ];

  return (
    <Container maxWidth="desktop">
      <Breadcrumbs breadcrumbs={breadcrumbs} />
      <Cards>
        {categories.categories.map((category: Category) => (
          <Card
            key={`${category.idCategory}${Math.random()}`}
            bgUrl={category.strCategoryThumb}
            name={category.strCategory}
            description={category.strCategoryDescription}
            href={`/recipes/${category.strCategory}`}
          />
        ))}
      </Cards>
    </Container>
  );
};

export default Recipes;
