import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import { ParsedUrlQuery } from 'querystring';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import { useRouter } from 'next/router';
import { RecipesEndpoints } from '../../../hooks/useFetch/enumerators';
import { Meal, Meals } from '../../../components/Cards/interface';
import Cards from '../../../components/Cards';
import Card from '../../../components/Card';
import Breadcrumbs from '../../../components/Breadcrumbs';

export const getStaticPaths: GetStaticPaths = async () => {
  const arr: string[] = [
    'Beef',
    'Chicken',
    'Dessert',
    'Lamb',
    'Miscellaneous',
    'Pasta',
    'Pork',
    'Seafood',
    'Side',
    'Starter',
    'Vegan',
    'Vegetarian',
    'Breakfast',
    'Goat',
  ];
  const paths = arr.map((dishes) => {
    return {
      params: { dishes },
    };
  });
  return { paths, fallback: 'blocking' };
};

interface IParams extends ParsedUrlQuery {
  dishes: string;
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { dishes } = context.params as IParams;
  const response = await fetch(
    `${RecipesEndpoints.getMealsByCategories}${dishes}`
  );
  const meals = await response.json();

  return {
    props: {
      meals,
      dishes,
    },
  };
};

interface DishesProps {
  meals: Meals;
  dishes: string;
}

const Dishes: NextPage<DishesProps> = ({ meals, dishes }) => {
  const router = useRouter();
  const { asPath } = router;

  const onClick = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    event.preventDefault();
    router.push('/recipes');
  };

  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      href="/recipes"
      onClick={onClick}
    >
      Recipes
    </Link>,
    <Typography key="2" color="text.primary">
      {dishes}
    </Typography>,
  ];

  return (
    <Container maxWidth="desktop">
      <Breadcrumbs breadcrumbs={breadcrumbs} />
      <Cards>
        {meals.meals.map((meal: Meal) => (
          <Card
            key={`${meal.idMeal}${Math.random()}`}
            bgUrl={meal.strMealThumb}
            name={meal.strMeal}
            href={`${asPath}/${meal.idMeal}`}
            textButton={'Select meal'}
          />
        ))}
      </Cards>
    </Container>
  );
};

export default Dishes;
