import { NextPage } from 'next';
import { useState, useCallback, useEffect } from 'react';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import { useRouter } from 'next/router';
import toast from 'react-hot-toast';
import Grid from '@mui/material/Grid';
import { brown } from '@mui/material/colors';
import { MoonLoader } from 'react-spinners';
import Image from 'next/image';
import Divider from '@mui/material/Divider';
import { useFetch } from '../../../../hooks/useFetch';
import { RecipesEndpoints } from '../../../../hooks/useFetch/enumerators';
import Breadcrumbs from '../../../../components/Breadcrumbs';
import Duration from '../../../../components/Duration';
import Yield from '../../../../components/Yield';

interface Meal {
  meals: Array<{
    [key: string]: string | null;
  }>;
}

const initialMeal: Meal = {
  meals: [
    {
      strMeal: 'Loading...',
      strInstructions: 'Loading...',
      strMealThumb: null,
    },
  ],
};

const Dish: NextPage = () => {
  const router = useRouter();
  const { dishes, dish } = router.query;
  const { onRequest, isLoading, error, clearError } = useFetch();
  const [meal, setMeal] = useState<Meal>(initialMeal);

  const { strMeal, strInstructions, strMealThumb } = meal.meals[0];

  const getMealById = useCallback(async () => {
    const result = await onRequest(`${RecipesEndpoints.getMealById}${dish}`);
    setMeal(result);
  }, [dish]);

  useEffect(() => {
    if (dish) {
      getMealById();
    }
  }, [dish]);

  useEffect(() => {
    toast.error(error);
    clearError();
  }, [error, clearError]);

  const onClickRecipes = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    event.preventDefault();
    router.push('/recipes');
  };

  const onClickDishes = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    event.preventDefault();
    router.push(`/recipes/${dishes}`);
  };

  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      href="/recipes"
      onClick={onClickRecipes}
    >
      Recipes
    </Link>,
    <Link
      underline="hover"
      key="2"
      color="inherit"
      href={`/recipes/${dishes}`}
      onClick={onClickDishes}
    >
      {dishes}
    </Link>,
    <Typography key="3" color="text.primary">
      strMeal
    </Typography>,
  ];
  return (
    <Container maxWidth="desktop">
      <Grid
        sx={{ display: 'grid', gridTemplateColumns: '1fr 1fr', columnGap: 8 }}
      >
        <Grid item>
          <Breadcrumbs breadcrumbs={breadcrumbs} />
          <Typography variant="h3" color={brown[900]} sx={{ marginBottom: 16 }}>
            {strMeal}
          </Typography>
          <Typography variant="h5" color={brown[900]}>
            {strInstructions}
          </Typography>
          <Duration />
          <Divider />
          <Yield />
        </Grid>
        <Grid
          item
          sx={{
            display: 'flex',
            height: strMealThumb !== null ? 'auto' : '584px',
            justifyContent: 'center',
            alignItems: 'center',
            '& > img': {
              position: 'relative!important',
              height: 'auto!important',
              alignSelf: 'flex-start',
            },
          }}
        >
          {strMealThumb !== null ? (
            <Image
              loader={() => strMealThumb as string}
              src={strMealThumb as string}
              layout="fill"
              objectFit="contain"
              alt={strMeal as string}
              loading="lazy"
            />
          ) : (
            <MoonLoader
              size={50}
              aria-label="Loading Spinner"
              data-testid="loader"
            />
          )}
        </Grid>
      </Grid>
    </Container>
  );
};

export default Dish;
