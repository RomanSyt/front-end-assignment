import { NextPage } from 'next';

export const getStaticProps = async () => {
  const response = await fetch('https://jsonplaceholder.typicode.com/posts');
  const posts = await response.json();

  return {
    props: {
      posts,
    },
  };
};

const About: NextPage<{ posts: any[] }> = ({ posts }) => (
  <>
    {posts.map((post) => (
      <span key={`${post.id}${Math.random()}`}>{post.userId}</span>
    ))}
  </>
);

export default About;
