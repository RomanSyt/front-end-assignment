export interface CardProps {
  bgUrl: string | null;
  name: string;
  description?: string;
  href: string;
  textButton?: string;
}
