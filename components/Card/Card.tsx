import { FC } from 'react';
import { Card as MuiCard } from '@mui/material';
import Box from '@mui/material/Box';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Skeleton from '@mui/material/Skeleton';
import { useRouter } from 'next/router';
import { CardProps } from './interface';

const Card: FC<CardProps> = ({
  bgUrl,
  name,
  description = null,
  href,
  textButton = 'Select category',
}) => {
  const router = useRouter();

  const onClick = () => {
    router.push(href);
  };

  return (
    <MuiCard>
      <Box
        sx={{
          display: 'flex',
          width: '100%',
          height: '140px',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {bgUrl !== null ? (
          <CardMedia
            component="img"
            height="100%"
            image={bgUrl as string}
            alt={name}
          />
        ) : (
          <Skeleton variant="rectangular" width="100%" height="140px" />
        )}
      </Box>
      <CardContent>
        <Typography
          gutterBottom
          variant="h5"
          component="div"
          sx={{
            width: '100%',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis',
          }}
        >
          {name}
        </Typography>
        {description !== null && (
          <Typography
            variant="body2"
            color="text.secondary"
            sx={{
              display: '-webkit-box',
              textAlign: 'justify',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              WebkitLineClamp: '3',
              WebkitBoxOrient: 'vertical',
              lineHeight: '1.5rem',
              height: '4.5rem',
            }}
          >
            {description}
          </Typography>
        )}
      </CardContent>
      <CardActions>
        <Button size="small" onClick={onClick}>
          {textButton}
        </Button>
      </CardActions>
    </MuiCard>
  );
};

export default Card;
