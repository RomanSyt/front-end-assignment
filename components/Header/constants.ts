interface route {
  name: string;
  href: string;
}

export const PAGES: route[] = [
  {
    name: 'shop',
    href: '/shop',
  },
  { name: 'recipes', href: '/recipes' },
  {
    name: 'learn',
    href: '/learn',
  },
  { name: 'about', href: '/about' },
  { name: 'blog', href: '/blog' },
];
export const OPTIONS: route[] = [
  { name: 'categories', href: '/' },
  { name: 'collections', href: '/' },
  { name: 'resources', href: '/' },
];

export const LOGO_WIDTH: number = 90;
export const INITIAL_LOGO_WIDTH: number = 2384;
export const INITIAL_LOGO_HEIGHT: number = 2596;

export const getLogoHeight = (logoWidth: number): number =>
  (logoWidth * INITIAL_LOGO_HEIGHT) / INITIAL_LOGO_WIDTH;
