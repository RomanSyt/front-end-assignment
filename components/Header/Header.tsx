import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { red, brown, amber } from '@mui/material/colors';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import { getLogoHeight, LOGO_WIDTH, OPTIONS, PAGES } from './constants';

const Header = () => {
  const router = useRouter();
  const { pathname } = router;
  return (
    <AppBar
      position="static"
      elevation={0}
      sx={{ bgcolor: 'inherit', marginBottom: '1rem' }}
    >
      <Container maxWidth="desktop">
        <Toolbar sx={{ position: 'relative' }} disableGutters>
          <List
            sx={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignContent: 'center',
              gap: 8,
            }}
            disablePadding
          >
            {PAGES.map((page) => (
              <ListItem
                key={`${page.name}${Math.random()}`}
                selected={new RegExp(`^\\${page.href}.*`).test(pathname)}
                sx={{
                  borderBottomWidth: '3px',
                  borderBottomStyle: 'solid',
                  borderBottomColor: 'inherit',
                  '&.Mui-selected': {
                    bgcolor: 'inherit',
                    borderBottomColor: red[600],
                  },
                  '& a': {
                    textTransform: 'uppercase',
                    textDecoration: 'none',
                    fontSize: '1rem',
                    fontWeight: '500',
                    color: brown[900],
                  },
                }}
                disablePadding
              >
                <NextLink key={`${page.name}${Math.random()}`} href={page.href}>
                  {page.name}
                </NextLink>
              </ListItem>
            ))}
          </List>
          <Box
            sx={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: LOGO_WIDTH,
              height: getLogoHeight(LOGO_WIDTH),
              zIndex: 1000,
              '& a': {
                display: 'inline-block',
                width: '100%',
                height: '100%',
                textIndent: -999999,
                backgroundImage: 'url(/assets/logo.png)',
                backgroundRepeat: 'no-repeat',
                backgroundSize: '100% 100%',
              },
            }}
          >
            <NextLink href="/">Logo</NextLink>
          </Box>
        </Toolbar>
      </Container>
      <Box sx={{ bgcolor: amber[50] }}>
        <Container maxWidth="desktop">
          <Toolbar disableGutters>
            <List
              sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignContent: 'center',
                gap: 4,
              }}
              disablePadding
            >
              {OPTIONS.map((option) => (
                <ListItem
                  key={`${option.name}${Math.random()}`}
                  sx={{
                    '&': {
                      textTransform: 'uppercase',
                      textDecoration: 'none',
                      color: brown[900],
                      fontSize: '0.8rem',
                      fontWeight: '400',
                    },
                  }}
                  disablePadding
                >
                  {option.name}
                </ListItem>
              ))}
            </List>
          </Toolbar>
        </Container>
      </Box>
    </AppBar>
  );
};
export default Header;
