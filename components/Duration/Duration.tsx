import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import AccessTimeOutlinedIcon from '@mui/icons-material/AccessTimeOutlined';
import { brown } from '@mui/material/colors';

const Duration = () => {
  return (
    <List
      sx={{
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        bgcolor: 'background.paper',
        '.MuiListItemText-primary': {
          textTransform: 'uppercase',
          color: brown[900],
        },
        '.MuiListItemText-secondary': {
          fontSize: '1.2rem',
          fontWeight: 600,
          color: brown[900],
        },
      }}
    >
      <ListItem disableGutters>
        <ListItemAvatar>
          <Avatar>
            <AccessTimeOutlinedIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Prep" secondary="10 mins" />
      </ListItem>
      <ListItem disableGutters>
        <ListItemText primary="Bake" secondary="1hr to 1hr 15mins" />
      </ListItem>
      <ListItem disableGutters>
        <ListItemText primary="Total" secondary="1hr 10mins" />
      </ListItem>
    </List>
  );
};

export default Duration;
