import React from 'react';

export interface BreadcrumbsProps {
  breadcrumbs: Array<React.ReactNode>;
}
