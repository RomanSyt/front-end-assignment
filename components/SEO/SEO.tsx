import Head from 'next/head';

const SEO = () => (
  <>
    <Head>
      <title>Website</title>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1 viewport-fit=cover"
      />
      <meta
        name="description"
        content="Property search & discovery reimagined"
      />

      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />

      <link rel="icon" href="/favicon.ico" />
    </Head>
  </>
);

export default SEO;
