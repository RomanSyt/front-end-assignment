import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import ScatterPlotRoundedIcon from '@mui/icons-material/ScatterPlotRounded';
import ListItemButton from '@mui/material/ListItemButton';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import { brown, red } from '@mui/material/colors';

const Yield = () => {
  return (
    <List
      sx={{
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        bgcolor: 'background.paper',
        '.MuiListItemText-primary': {
          textTransform: 'uppercase',
          color: brown[900],
        },
        '.MuiListItemText-secondary': {
          fontSize: '1.2rem',
          fontWeight: 600,
          color: brown[900],
        },
        '.MuiButtonBase-root': {
          borderWidth: '1px',
          borderStyle: 'solid',
          borderColor: red[400],
        },
      }}
    >
      <ListItem disableGutters>
        <ListItemAvatar>
          <Avatar>
            <ScatterPlotRoundedIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="yield"
          secondary="1 loaf, 12 generous servings"
        />
      </ListItem>
      <ListItem disableGutters sx={{ minWidth: '315px', columnGap: 1 }}>
        <ListItemButton>
          <AddRoundedIcon />
          <ListItemText primary="SAVE RECIPE" />
        </ListItemButton>
        <ListItemButton>
          <LocalPrintshopOutlinedIcon />
          <ListItemText primary="PRINT" />
        </ListItemButton>
      </ListItem>
    </List>
  );
};

export default Yield;
