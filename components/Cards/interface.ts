import React from 'react';

export interface Category {
  idCategory: string;
  strCategory: string;
  strCategoryDescription: string;
  strCategoryThumb: string | null;
}

export interface Meal {
  idMeal: string;
  strMeal: string;
  strMealThumb: string | null;
}

export interface CardsProps {
  children: React.ReactNode;
}

export type Categories = { categories: Array<Category> };
export type Meals = { meals: Array<Meal> };
