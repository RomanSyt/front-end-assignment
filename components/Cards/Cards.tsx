import { FC } from 'react';
import { CardsProps } from './interface';
import Grid from '@mui/material/Grid';

const Cards: FC<CardsProps> = ({ children }) => (
  <Grid
    container
    sx={{
      display: 'grid',
      gridTemplateColumns: '1fr 1fr 1fr',
      columnGap: '1rem',
      rowGap: '1rem',
    }}
  >
    {children}
  </Grid>
);

export default Cards;
